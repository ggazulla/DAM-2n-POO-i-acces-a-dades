#### Polimorfisme

El **polimorfisme** consisteix en la capacitat que tenen els objectes de ser
de més d'un tipus a la vegada. La utilitat del polimorfisme és poder
treballar de forma uniforme amb objectes que compleixin una certa
interfície, sense saber exactament de quina classe són.

Per exemple, un mètode que cerqui el valor màxim d'una col·lecció en
tindrà prou amb utilitzar els mètodes de la interfície *Collection*, i
podrà actuar igualment sobre un *ArrayList*, un *LinkedList*, o un
*HashSet*, perquè tots aquests objectes són a la vegada de tipus
*Collection*. Internament, els mètodes que es cridin actuaran diferent
depenent de quin tipus concret sigui l'objecte, però això quedarà ocult
al nostre mètode.

Un objecte sempre és del tipus de dades de la classe de la qual s'ha
creat. A més, l'objecte és del tipus de la classe mare, i de la mare
d'aquesta, etc. En particular, tot objecte és de tipus *Object*, perquè
totes les classes hereten sempre de *Object*. Igualment, un objecte
sempre és del tipus de totes les interfícies que implementi la seva
classe.

Per exemple, un objecte creat com un *ArrayList* és dels següents tipus:

 * Per ser de la classe o derivar de la classe: *ArrayList*, *AbstractList*,
*AbstractCollection*, *Object*.

 * Per implementar la interfície: *Serializable*, *Cloneable*, *Iterable*,
*Collection*, *List*, *RandomAccess*.

Podem assignar un objecte d'un tipus a una referència d'un tipus més
general. Per exemple, podem assignar a una referència de tipus *List* un
objecte de tipus *ArrayList*, perquè tots els *ArrayList* són *List*:

```java
List<String> llista = new ArrayList<String>();
```

Per assignar un objecte d'un tipus a una referència d'un tipus més
concret cal fer-ho amb un *cast* i estar segurs que fem una operació
correcte, o es produirà una excepció en temps d'execució. Per exemple,
si tenim un objecte de tipus *List*, no té perquè ser de tipus
*ArrayList*, i per tant, aquesta assignació no és immediata:

```java
public void metode(List<String> unaLlista) {
   ArrayList<String> unArrayList = unaLlista; // INCORRECTE!
   ArrayList<String> unArrayList = (ArrayList<String>) unaLlista; // PERILLÓS, pot ser mentida!
```

Podem comprovar si un objecte és d'un determinat tipus amb l'operador
instanceof:

```java
if (unaLlista instanceof ArrayList<String>) {
   System.out.println("La llista és de tipus ArrayList");
   ArrayList<String> unArratList = (ArrayList<String>) unaLlista; // Segur que ara funciona
```